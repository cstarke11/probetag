package router

import (
	"net/http"
	"time"

	"github.com/google/uuid"
	"worker/logging"
)

const XRequestIDHeader = "X-Request-Id"
const XForwardedForHeader = "X-Forwarded-For"
const UserAgentHeader = "User-Agent"
const AuthHeader = "Authorization"

type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func NewLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	return &loggingResponseWriter{w, http.StatusOK}
}

func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

func LoggingResponseWriterMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// track time for request processing
		startTime := time.Now().UTC()

		// inject custom interface to retrieve response code
		lrw := NewLoggingResponseWriter(w)
		next.ServeHTTP(lrw, r)

		endTime := time.Now().UTC()
		totalTime := endTime.Sub(startTime)

		// access log
		logging.InfoWithFields("", logging.LogFields{
			"response_code":           lrw.statusCode,
			"request_uri":             r.RequestURI,
			"request_method":          r.Method,
			"request_protocol":        r.Proto,
			"request_id":              r.Header.Get(XRequestIDHeader),
			"request_x_forwarded_for": r.Header.Get(XForwardedForHeader),
			"request_user_agent":      r.Header.Get(UserAgentHeader),
			"duration":                totalTime,
		})
	})
}

// Enables Cross-Origin Resource Sharing (CORS) for everyone (*)
func CrossOriginResourceSharingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		next.ServeHTTP(w, r)
	})
}

// Re-Set the X-Request-Id header
func ForwardRequestIDMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rid := r.Header.Get(XRequestIDHeader)
		if rid == "" {
			rid = uuid.NewString()
			r.Header.Set("X-Request-ID", rid)
		}
		w.Header().Add(XRequestIDHeader, rid)
		next.ServeHTTP(w, r)
	})
}
