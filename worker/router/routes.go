package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"worker/controllers/health"
)

func SetupRouting(isDebugMode bool) *mux.Router {

	router := mux.NewRouter()
	router.Use(LoggingResponseWriterMiddleware)
	if isDebugMode {
		router.Use(CrossOriginResourceSharingMiddleware)
	}
	router.Use(ForwardRequestIDMiddleware)

	subRouterHealth := router.PathPrefix("/health").Subrouter()
	subRouterHealth.
		Path("").
		Methods(http.MethodGet).
		HandlerFunc(health.HealthCtrl).
		Name("Health")

	subRouterHealth.
		Path("/readiness").
		Methods(http.MethodGet).
		HandlerFunc(health.HealthReadinessCtrl).
		Name("HealthReadiness")

	return router
}
