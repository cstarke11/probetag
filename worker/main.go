package main

import (
	"net/http"

	"worker/clients/pgsql"
	"worker/config"
	"worker/logging"
	"worker/router"
)

func main() {
	var isDebugMode bool
	if config.GetEnv(config.ConfigParamDebug, "false") == "true" {
		isDebugMode = true
		logging.EnableDebug()
	}

	// Configure Postgresql client
	databaseHost := config.GetEnv(config.ConfigParamDatabaseHost, "127.0.0.1")
	databasePort := config.GetEnv(config.ConfigParamDatabasePort, "5432")
	databaseName := config.GetEnv(config.ConfigParamDatabaseName, "db")
	databaseUsername := config.GetEnv(config.ConfigParamDatabaseUsername, "worker")
	databasePassword := config.GetEnv(config.ConfigParamDatabasePassword, "worker")
	databaseMaxOpenConnections := config.GetEnvInt(config.ConfigParamDatabaseMaxOpenConnections, 150)
	databaseTablePrefix := config.GetEnv(config.ConfigParamDatabaseTablePrefix, "worker.")
	config.DataBaseClient = pgsql.NewClient(databaseHost, databasePort, databaseName, databaseUsername,
		databasePassword, databaseMaxOpenConnections, databaseTablePrefix)
	config.DataBaseClient.Connect()

	// setup API routing
	config.Router = router.SetupRouting(isDebugMode)
	listenAddr := config.GetEnv(config.ConfigParamListenAddress, ":1234")

	logging.Infof("Starting to serve HTTP on %s", listenAddr)
	logging.Fatal(http.ListenAndServe(listenAddr, config.Router))
}
