package pgsql

import (
	"errors"
	"fmt"

	"worker/logging"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

type Client struct {
	host               string
	port               string
	database           string
	username           string
	password           string
	maxOpenConnections int
	tablePrefix        string
	conn               *gorm.DB
}

func NewClient(host string, port string, database string, username string, password string, maxOpenConnections int, tablePrefix string) *Client {
	return &Client{host, port, database, username, password, maxOpenConnections, tablePrefix, nil}
}

func (c *Client) Connect() (*gorm.DB, error) {
	connection := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s", c.host, c.port, c.username, c.database, c.password)
	logging.Infof("Connecting to Postgres on %s", connection)
	var err error
	c.conn, err = gorm.Open(postgres.Open(connection), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: c.tablePrefix,
		}})
	if err != nil {
		return nil, err
	}
	dbSQL, err := c.conn.DB()
	if err != nil {
		return nil, err
	}
	dbSQL.SetMaxOpenConns(c.maxOpenConnections)
	return c.conn, nil
}

func (c *Client) GetConn() *gorm.DB {
	if c.conn == nil {
		c.Connect()
	}
	return c.conn
}

func (c *Client) Disconnect() error {
	if c.conn != nil {
		dbSQL, err := c.conn.DB()
		if err != nil {
			return err
		}
		dbSQL.Close()
	}
	return nil
}

func (c *Client) GetTablePrefix() string {
	return c.tablePrefix
}

func (c *Client) ClearDB() {
	c.conn.Migrator().DropTable()
}

func (c *Client) Health() error {
	// check for availability and auth by using
	dbSQL, err := c.conn.DB()
	if err != nil {
		return errors.New("Postgres is disconnected.")
	}
	if err = dbSQL.Ping(); err != nil {
		return errors.New("Postgres is disconnected.")
	}
	return nil
}
