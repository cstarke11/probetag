package config

import (
	"os"
	"strconv"

	"worker/logging"
)

const (
	ConfigParamDebug                      = "DEBUG"
	ConfigParamListenAddress              = "LISTEN_ADDR"
	ConfigParamDatabaseName               = "DB_NAME"
	ConfigParamDatabaseHost               = "DB_HOST"
	ConfigParamDatabasePort               = "DB_PORT"
	ConfigParamDatabaseUsername           = "DB_USERNAME"
	ConfigParamDatabasePassword           = "DB_PASSWORD"
	ConfigParamDatabaseMaxOpenConnections = "DB_MAX_OPEN_CONNECTIONS"
	ConfigParamDatabaseTablePrefix        = "DB_TABLE_PREFIX"
)

var osLookupEnv = os.LookupEnv

func GetEnv(key, fallback string) string {
	if value, found := osLookupEnv(key); found {
		return value
	}
	return fallback
}

// GetEnvInt gracefully reads the environment variable with the given key and returns its value as int.
// If either the environment variable is not set or if it cannot be converted to an int, then it returns the fallback.
// In case of a conversion error, an error is logged.
func GetEnvInt(key string, fallback int) int {
	strValue, found := osLookupEnv(key)
	if !found {
		return fallback
	}
	value, err := strconv.Atoi(strValue)
	if err != nil {
		logging.Errorf("environment variable %v is not a number: %v", key, strValue)
		return fallback
	}
	return value
}
