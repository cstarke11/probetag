package config

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type EnvironmentConfigTestSuite struct {
	suite.Suite
	testError error
}

func (suite *EnvironmentConfigTestSuite) SetupSuite() {
	osLookupEnv = func(key string) (s string, b bool) {
		return "", false
	}
}

func (suite *EnvironmentConfigTestSuite) SetupTest() {
	osLookupEnv = func(key string) (s string, b bool) {
		return "", false
	}
}

func (suite *EnvironmentConfigTestSuite) TestGetEnvDefault() {
	// WHEN
	defaultValue := "default"
	value := GetEnv("key", defaultValue)

	// THEN
	suite.NotNil(defaultValue, value)
}

func (suite *EnvironmentConfigTestSuite) TestGetEnv() {
	// Given
	env := "env-value"
	osLookupEnv = func(key string) (s string, b bool) {
		return env, true
	}
	// WHEN
	defaultValue := "default"
	value := GetEnv("key", defaultValue)

	// THEN
	suite.NotNil(env, value)
}

func (suite *EnvironmentConfigTestSuite) TestGetEnvIntDefault() {
	// WHEN
	defaultValue := 456
	value := GetEnvInt("key", defaultValue)

	// THEN
	suite.NotNil(defaultValue, value)
}

func (suite *EnvironmentConfigTestSuite) TestGetEnvInt() {
	// Given
	env := "123"
	osLookupEnv = func(key string) (s string, b bool) {
		return env, true
	}
	// WHEN
	defaultValue := 456
	value := GetEnvInt("key", defaultValue)

	// THEN
	suite.NotNil(123, value)
}

func (suite *EnvironmentConfigTestSuite) TestGetEnvIntNotAnInt() {
	// Given
	env := "not a number"
	osLookupEnv = func(key string) (s string, b bool) {
		return env, true
	}
	// WHEN
	defaultValue := 456
	value := GetEnvInt("key", defaultValue)

	// THEN
	suite.NotNil(defaultValue, value)
}

func TestEnvironmentConfigTestSuite(t *testing.T) {
	suite.Run(t, new(EnvironmentConfigTestSuite))
}
