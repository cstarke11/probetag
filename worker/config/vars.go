package config

import (
	"github.com/gorilla/mux"
	"worker/clients/pgsql"
)

var (
	DataBaseClient  *pgsql.Client     // Postgres Client
	Router          *mux.Router
)
