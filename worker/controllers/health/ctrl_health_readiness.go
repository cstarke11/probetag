package health

import (
	"fmt"
	"net/http"

	"worker/config"
	"worker/logging"
)

func HealthReadinessCtrl(w http.ResponseWriter, r *http.Request) {
	err := func() error {
		err := config.DataBaseClient.Health()
		if err != nil {
			return fmt.Errorf("Database client error: %s", err.Error())
		}

		return nil
	}()
	if err != nil {
		logging.Error(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.WriteHeader(http.StatusOK)
	}

	w.Write(nil)
}
