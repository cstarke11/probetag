package health

import (
	"net/http"
)

func HealthCtrl(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write(nil)
}
