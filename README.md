# probetag

Welcome to your trail day (Probetag)!

Your task is to build a REST-API for the "worker" service,
which can be found in the `docker-compose.yaml`.

The API should:
1. Allow its users simple CRUD functionality around Docker containers using the forwarded socket
2. Let only authenticated users do writable actions
3. Persist the list of containers and their state in the postgres-DB
4. Apply container state with resources upon restart (also stop containers that should not be up)

CRUD functionality hereby means: Creating containers from images with a given command,
listing, starting, stopping and removing them.

The authentication can be as simple as possible.

As the developer of this API, you shall implement the above
specifications, share requests through the nginx proxy
in a round-robin style across multiple instances of the worker.

Lastly, please write appropriate tests.
